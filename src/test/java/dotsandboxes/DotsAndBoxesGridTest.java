package dotsandboxes;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class DotsAndBoxesGridTest {
    /*
     * Because Test classes are classes, they can have fields, and can have static fields.
     * This field is a logger. Loggers are like a more advanced println, for writing messages out to the console or a log file.
     */
    private static final Logger logger = LogManager.getLogger(DotsAndBoxesGridTest.class);

    /*
     * Tests are functions that have an @Test annotation before them.
     * The typical format of a test is that it contains some code that does something, and then one
     * or more assertions to check that a condition holds.
     *
     * This is a dummy test just to show that the test suite itself runs
     */
    @Test
    public void testTestSuiteRuns() {
        logger.info("Dummy test to show the test suite runs");
        assertTrue(true);
    }

    // Test cases for known bugs

    @Test
    public void testSquareCompletionBug() {
        logger.info("Testing square completion logic");

        // Create a new game grid
        DotsAndBoxesGrid grid = new DotsAndBoxesGrid(3, 3, 2);

        // Draw lines to form a square
        grid.drawHorizontal(0, 0, 1); // Top horizontal line
        grid.drawVertical(0, 0, 1);   // Left vertical line
        grid.drawHorizontal(0, 1, 1); // Bottom horizontal line
        grid.drawVertical(1, 0, 1);   // Right vertical line

        // Test if the square at (0, 0) is complete
        boolean isComplete = grid.boxComplete(0, 0);
        
        // The test should pass now that the logic is fixed
        assertTrue(isComplete, "The square at (0, 0) should be complete.");
    }

    @Test
    public void testDrawLineTwiceThrowsException() {
        logger.info("Testing drawing the same line twice throws exception");

        // Create a new game grid
        DotsAndBoxesGrid grid = new DotsAndBoxesGrid(3, 3, 2);

        // Draw a line once
        grid.drawHorizontal(0, 0, 1);
        
        // Attempt to draw the same line again
        assertThrows(IllegalStateException.class, () -> {
            grid.drawHorizontal(0, 0, 1);
        }, "Drawing the same horizontal line twice should throw IllegalStateException.");
        
        // Draw a vertical line once
        grid.drawVertical(0, 0, 1);
        
        // Attempt to draw the same vertical line again
        assertThrows(IllegalStateException.class, () -> {
            grid.drawVertical(0, 0, 1);
        }, "Drawing the same vertical line twice should throw IllegalStateException.");
    }
}
